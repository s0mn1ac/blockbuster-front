import { Component } from '@angular/core';
import { AuthService } from '../login/login/auth.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    title: string = 'Blockbuster';

    constructor(public authService: AuthService, private router: Router) {}

    logout(): void {
        this.authService.logout();
        this.router.navigate(['/login']);
        swal.fire('Cierre de sesión correcto', 'Has cerrado sesión correctamente', 'success');
    }

}