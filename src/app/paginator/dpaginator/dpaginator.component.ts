import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'paginator-desarrolladoras',
  templateUrl: './dpaginator.component.html',
})
export class DpaginatorComponent implements OnInit, OnChanges {

  @Input() dpaginador: any;
  pages: number[];
  from: number;
  to: number;

  constructor() { }

  ngOnInit(): void {
    this.iniciarPaginadorDesarrolladoras();
  }

  ngOnChanges(changes: SimpleChanges): void {
    let dpaginadorActualizado = changes['dpaginador'];
    if(dpaginadorActualizado.previousValue) {
      this.iniciarPaginadorDesarrolladoras();
    }
  }

  private iniciarPaginadorDesarrolladoras(): void {
    this.from = Math.min(Math.max(1, this.dpaginador.number - 4), this.dpaginador.totalPages - 5);
    this.to = Math.max(Math.min(this.dpaginador.totalPages, this.dpaginador.number + 4), 6);
    if(this.dpaginador.totalPages > 5) {
      this.pages = new Array(this.from - this.to + 1).fill(0).map((valor, indice) => indice + this.to);
    } else {
      this.pages = new Array(this.dpaginador.totalPages).fill(0).map((valor, indice) => indice + 1);
    }
  }

}
