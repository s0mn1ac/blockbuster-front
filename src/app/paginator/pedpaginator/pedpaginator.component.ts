import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'paginator-pedidos',
  templateUrl: './pedpaginator.component.html',
})
export class PedpaginatorComponent implements OnInit, OnChanges {

  @Input() pedidosPaginator: any;
  pages: number[];
  from: number;
  to: number;

  constructor() { }

  ngOnInit(): void {
    this.startPedidosPaginator();
  }

  ngOnChanges(changes: SimpleChanges): void {
    let pagAct = changes['pedidosPaginator'];
    if(pagAct.previousValue) {
      this.startPedidosPaginator();
    }
  }

  private startPedidosPaginator(): void {
    this.from = Math.min(Math.max(1, this.pedidosPaginator.number - 4), this.pedidosPaginator.totalPages - 5);
    this.to = Math.max(Math.min(this.pedidosPaginator.totalPages, this.pedidosPaginator.number + 4), 6);
    if(this.pedidosPaginator.totalPages > 5) {
      this.pages = new Array(this.from - this.to + 1).fill(0).map((valor, indice) => indice + this.to);
    } else {
      this.pages = new Array(this.pedidosPaginator.totalPages).fill(0).map((valor, indice) => indice + 1);
    }
  }

}
