import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'paginator-tiendas',
  templateUrl: './tpaginator.component.html',
})
export class TpaginatorComponent implements OnInit, OnChanges {

  @Input() tpaginador: any;
  pages: number[];
  from: number;
  to: number;

  constructor() { }

  ngOnInit(): void {
    this.iniciarPaginadorTiendas();
  }

  ngOnChanges(changes: SimpleChanges): void {
    let tpaginadorActualizado = changes['tpaginador'];
    if(tpaginadorActualizado.previousValue) {
      this.iniciarPaginadorTiendas();
    }
  }

  private iniciarPaginadorTiendas(): void {
    this.from = Math.min(Math.max(1, this.tpaginador.number - 4), this.tpaginador.totalPages - 5);
    this.to = Math.max(Math.min(this.tpaginador.totalPages, this.tpaginador.number + 4), 6);
    if(this.tpaginador.totalPages > 5) {
      this.pages = new Array(this.from - this.to + 1).fill(0).map((valor, indice) => indice + this.to);
    } else {
      this.pages = new Array(this.tpaginador.totalPages).fill(0).map((valor, indice) => indice + 1);
    }
  }

}
