import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'paginator-juegos',
  templateUrl: './jpaginator.component.html',
})
export class JpaginatorComponent implements OnInit, OnChanges {

  @Input() jpaginador: any;
  pages: number[];
  from: number;
  to: number;

  constructor() { }

  ngOnInit(): void {
    this.iniciarPaginadorJuegos();
  }

  ngOnChanges(changes: SimpleChanges): void {
    let jpaginadorActualizado = changes['jpaginador'];
    if(jpaginadorActualizado.previousValue) {
      this.iniciarPaginadorJuegos();
    }
  }

  private iniciarPaginadorJuegos(): void {
    this.from = Math.min(Math.max(1, this.jpaginador.number - 4), this.jpaginador.totalPages - 5);
    this.to = Math.max(Math.min(this.jpaginador.totalPages, this.jpaginador.number + 4), 6);
    if(this.jpaginador.totalPages > 5) {
      this.pages = new Array(this.from - this.to + 1).fill(0).map((valor, indice) => indice + this.to);
    } else {
      this.pages = new Array(this.jpaginador.totalPages).fill(0).map((valor, indice) => indice + 1);
    }
  }

}
