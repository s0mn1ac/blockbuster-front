import { Component, OnInit } from "@angular/core";
import { Cliente } from "./cliente";
import { ClienteService } from "./cliente.service";
import swal from 'sweetalert2';
import { tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalService } from './cdetails/modal.service';
import { AuthService } from '../login/login/auth.service';

@Component({
  selector: "app-clientes",
  templateUrl: "./clientes.component.html",
  styleUrls: ["./clientes.component.css"]
})
export class ClientesComponent implements OnInit {
  
  clientes: Cliente[];
  paginador: any;
  showID: boolean;
  clienteSelected: Cliente;
  
  constructor(private clienteService: ClienteService, private activatedRoute: ActivatedRoute, private modalService: ModalService, private authService: AuthService) {}
  
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');
      if(!page) {
        page = 0;
      }
      this.clienteService
        .getClientes(page)
        /*
        .pipe(
          tap(response => {
            console.log('ClientesComponent: tap 3');
            (response.content as Cliente[]).forEach(cliente => {
              console.log(cliente.nombre);
            });
          })
        )
        */
        .subscribe(response => {
          this.clientes = response.content as Cliente[];
          this.paginador = response;
        });
    });
    this.modalService.notifyUpload.subscribe(cliente => {
      this.clientes = this.clientes.map(clienteOriginal => {
        if(cliente.id == clienteOriginal.id){
          clienteOriginal.img = cliente.img;
        }
        return clienteOriginal;
      })
    })
  }

  switchID(): void {
    this.showID = !this.showID;
  }

  delete(cliente: Cliente): void {
    swal.fire({
      title: '¿Estás seguro?',
      text: "¡No podrás revertir los cambios!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '¡Sí, eliminar!',
      cancelButtonText: '¡No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.clienteService.delete(cliente.id).subscribe(
          response => {
            this.clientes = this.clientes.filter(cli => cli !== cliente)
            swal.fire(
              '¡Eliminado!',
              `El cliente ${cliente.nombre} ha sido eliminado con éxito`,
              'success'
            )
          }
        )  
      } 
    })
  }

  openDetails(cliente: Cliente) {
    this.clienteSelected = cliente;
    this.modalService.openModal();
  }
  
}