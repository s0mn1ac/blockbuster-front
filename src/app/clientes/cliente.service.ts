import { Injectable } from "@angular/core";
import { formatDate } from '@angular/common';
import { Cliente } from "./cliente";
import { Observable, throwError } from "rxjs";
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent } from '@angular/common/http';
import { Rol } from '../roles/rol';
import { map, catchError, tap } from "rxjs/operators";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from '../login/login/auth.service';

@Injectable()
export class ClienteService {
  
  private url: string = 'http://localhost:8090/clientes';
  private urlRoles: string = 'http://localhost:8090/roles';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  
  constructor(private http: HttpClient, private router: Router, private authService: AuthService) {}

  private addHeaderAuthorization() {
    let token = this.authService.token;
    if(token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isAuthorized(e): boolean {
    
    if(e.status == 401) {
      this.router.navigate(['/login']);
      return true;
    }

    if(e.status == 403) {
      this.router.navigate(['/pedidos']);
      swal.fire('Acceso denegado', 'Este apartado solamente está disponible para administradores', 'warning');
      return true;
    }

    return false;

  }
 
  getCliente(id): Observable<Cliente> {
    return this.http.get<Cliente>(this.url + '/' + id, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {

        if(this.isAuthorized(e)) {
          return throwError(e);
        }

        this.router.navigate(['/clientes']);
        swal.fire('Error al editar', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  getClientes(page: number): Observable<any> {
    return this.http.get<Cliente[]>(this.url + '/page/' + page, {headers: this.addHeaderAuthorization()}).pipe(
      /*
      tap(
        (response: any) => {
          console.log('ClienteService: tap 1');
          (response.content as Cliente[]).forEach(cliente => {
            console.log(cliente.nombre);
          });
        }
      ),
      */
      catchError(e => {

        if(this.isAuthorized(e)) {
          return throwError(e);
        }
      }),

      map(
        (response: any) => {
          (response.content as Cliente[]).map(cliente => {
            cliente.fechaNac = formatDate(cliente.fechaNac, 'EEEE dd, MMMM yyyy', 'es');
            return cliente;
          });
          return response;
        }
      )/*,
      tap(
        (response: any) => {
          console.log('ClienteService: tap 2');
          (response.content as Cliente[]).forEach(cliente => {
            console.log(cliente.nombre);
          })
        }
      )*/
    );
  }

  /*
  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.url).pipe(
      map(
        response => {
          let clientes = response as Cliente[];
          return clientes.map(cliente => {
            //cliente.nombre = cliente.nombre.toUpperCase();
            cliente.fechaNac = formatDate(cliente.fechaNac, 'EEEE dd, MMMM yyyy', 'es');
            return cliente;
          })
        }
      )
    );
  }
  */

  create(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.url, cliente, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {

        if(this.isAuthorized(e)) {
          return throwError(e);
        }

        if(e.error.exception == 'MethodArgumentNotValidException') {
          return throwError(e);
        }

        swal.fire('Error al crear el cliente', e.error.message, 'error');
        return throwError(e);

      })
    );
  }

  update(cliente: Cliente): Observable<Cliente> {
    return this.http.put<Cliente>(this.url + '/' + cliente.id, cliente, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {

        if(this.isAuthorized(e)) {
          return throwError(e);
        }

        if(e.error.exception == 'MethodArgumentNotValidException') {
          return throwError(e);
        }

        swal.fire('Error al editar el cliente', e.error.message, 'error');
        return throwError(e);

      })
    );
  }

  delete(id): Observable<Cliente> {
    return this.http.delete<Cliente>(`${this.url}/${id}`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {

        if(this.isAuthorized(e)) {
          return throwError(e);
        }

        swal.fire('Error al eliminar el cliente', e.error.message, 'error');
        return throwError(e);

      })
    );
  }

  getRoles(): Observable<Rol[]> {
    return this.http.get<Rol[]>(this.urlRoles, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

  uploadImg(file: File, id): Observable<HttpEvent<{}>> {
    
    let formData = new FormData();
    formData.append("file", file);
    formData.append("id", id);

    let httpHeaders = new HttpHeaders();
    let token = this.authService.token;
    if(token != null) {
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    
    const req = new HttpRequest('POST', `${this.url}/upload`, formData, {
      reportProgress: true,
      headers: httpHeaders
    });
    
    //console.log(req);
    return this.http.request(req).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );

  }
  
}