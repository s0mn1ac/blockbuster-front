import { Rol } from 'src/app/roles/rol';

export class Cliente {
    id: number;
    documento: string;
    nombre: string;
    fechaNac: string;
    correo: string;
    user: string;
    pass: string;
    roles: Rol[];
    img: string;
}
