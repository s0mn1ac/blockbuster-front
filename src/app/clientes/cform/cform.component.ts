import { Component, OnInit } from '@angular/core';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Rol } from 'src/app/roles/rol';

@Component({
  selector: 'app-cform',
  templateUrl: './cform.component.html',
  styleUrls: ['./cform.component.css']
})
export class CFormComponent implements OnInit {
  
  roles: Rol[];
  cliente: Cliente = new Cliente();
  titulo: string = 'Crear Cliente';
  errores: string[];
  showEye: boolean;
  
  constructor(private clienteService: ClienteService, private router: Router, private activatedRoute: ActivatedRoute) { }
  
  ngOnInit(): void {
    this.cargar();
    this.cargarRoles();
  }

  public cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id) {
        this.clienteService.getCliente(id).subscribe( (cliente) => this.cliente = cliente )
      }
    })
  }

  private cargarRoles(): void {
    this.clienteService.getRoles().subscribe(roles => this.roles = roles);
  }

  public compararRol(o1: Rol, o2: Rol): boolean {
    return o1 == null || o2 == null ? false : o1.nombre == o2.nombre;
  }

  public create(): void {
    this.clienteService.create(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes'])
        swal.fire('Nuevo cliente', `¡Cliente ${cliente.nombre} creado con éxito!`, 'success')
      },
      e => {
        this.errores = e.error.valids as string[];
      }
    );
  }

  public update(): void {
    this.clienteService.update(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes'])
        swal.fire('Cliente actualizado', `¡Cliente ${cliente.nombre} actualizado con éxito!`, 'success')
      },
      e => {
        this.errores = e.error.valids as string[];
      }
    )
  }

  togglePwd(): void {
    let inputNombre: HTMLInputElement = document.querySelector('#toggle');
    if(inputNombre.type == 'password') {
      inputNombre.type = 'text';
    } else {
      inputNombre.type = 'password';
    }
    this.showEye = !this.showEye;
  }

}