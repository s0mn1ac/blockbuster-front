import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { ModalService } from './modal.service';

@Component({
  selector: 'details-cliente',
  templateUrl: './cdetails.component.html',
  styleUrls: ['./cdetails.component.css']
})
export class CdetailsComponent implements OnInit {

  titulo: string = "Detalle del cliente";
  @Input() cliente: Cliente;
  imgSelected: File;
  progreso: number = 0;

  constructor(private clienteService: ClienteService, private router: Router, public modalService: ModalService) { }

  ngOnInit(): void {

    // Lo de abajo ya no es necesario al tener inyectado arriba el cliente
    /*
    this.activatedRoute.paramMap.subscribe(params => {
      let id: number = +params.get('id');
      if(id) {
        this.clienteService.getCliente(id).subscribe(cliente => {
          this.cliente = cliente;
          //console.log(this.cliente);
        });
      }
    })
    */
  }

  selectImg(event) {
    this.imgSelected = event.target.files[0];
    this.progreso = 0;
    if(this.imgSelected.type.indexOf('image') < 0) {
      swal.fire('Error en subida', 'Debes seleccionar una foto.', 'error');
      this.imgSelected = null;
    }
  }

  subirFoto() {
    if(!this.selectImg) {
      swal.fire('Error en subida', 'Debes seleccionar una foto.', 'error');
    } else {
      this.clienteService.uploadImg(this.imgSelected, this.cliente.id).subscribe(
      event => {
        if(event.type === HttpEventType.UploadProgress) {
          this.progreso = Math.round((event.loaded/event.total)*100);
        } else if(event.type === HttpEventType.Response) {
          let response: any = event.body;
          this.cliente = response as Cliente;
          this.modalService.notifyUpload.emit(this.cliente);
          swal.fire('Subida completada', '¡La foto se ha subido con éxito!', 'success');
        }
      })
    }
  }

  closeDetails() {
    this.modalService.closeModal();
    this.imgSelected = null;
    this.progreso = 0;
  }

}
