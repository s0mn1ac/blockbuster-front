import { Component, OnInit } from '@angular/core';
import { Tienda } from '../../tienda'
import { TiendaService } from '../../tienda.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-tform',
  templateUrl: './tform.component.html',
  styleUrls: ['./tform.component.css']
})
export class TformComponent implements OnInit {

  tienda: Tienda = new Tienda();
  titulo: string = 'Crear Tienda';
  errores: string[];

  constructor(private tiendaService: TiendaService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargar();
  }

  public cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id) {
        this.tiendaService.getTienda(id).subscribe( (tienda) => this.tienda = tienda )
      }
    })
  }

  public create(): void {
    this.tiendaService.create(this.tienda).subscribe(
      tienda => {
        this.router.navigate(['/tiendas'])
        swal.fire('Nueva tienda', `¡Tienda ${tienda.nombre} creada con éxito!`, 'success')
      },
      e => {
        this.errores = e.error.valids as string[];
      }
    );
  }

  public update(): void {
    this.tiendaService.update(this.tienda).subscribe(
      tienda => {
        this.router.navigate(['/tiendas'])
        swal.fire('Tienda actualizada', `¡Tienda ${tienda.nombre} actualizada con éxito!`, 'success')
      },
      e => {
        this.errores = e.error.valids as string[];
      }
    )
  }

}
