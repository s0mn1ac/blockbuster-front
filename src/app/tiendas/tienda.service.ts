import { Injectable } from "@angular/core";
import { formatDate } from '@angular/common';
import { Tienda } from "./tienda";
import { Observable, throwError } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from "rxjs/operators";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from '../login/login/auth.service';

@Injectable()
export class TiendaService {
  
  private url: string = 'http://localhost:8090/tiendas';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  
  constructor(private http: HttpClient, private router: Router, private authService: AuthService) {}

  private addHeaderAuthorization() {
    let token = this.authService.token;
    if(token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isAuthorized(e): boolean {
    
    if(e.status == 401) {
      this.router.navigate(['/login']);
      return true;
    }

    if(e.status == 403) {
      this.router.navigate(['/pedidos']);
      swal.fire('Acceso denegado', 'Este apartado solamente está disponible para administradores', 'warning');
      return true;
    }

    return false;

  }

  getTienda(id): Observable<Tienda> {
    return this.http.get<Tienda>(this.url + '/' + id, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        this.router.navigate(['/tiendas']);
        swal.fire('Error al editar', e.error.message, 'error');
        return throwError(e);
      })
    );
  }
  
  getTiendas(page: number): Observable<any> {
    return this.http.get<Tienda[]>(this.url + '/page/' + page, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
      }),
      map(
        (response: any) => {
          (response.content as Tienda[]).map(tienda => {
            return tienda;
          });
          return response;
        }
      )
    );
  }

  create(tienda: Tienda): Observable<Tienda> {
    return this.http.post<Tienda>(this.url, tienda, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        if(e.error.exception == 'MethodArgumentNotValidException') {
          return throwError(e);
        }
        swal.fire('Error al crear la tienda', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  update(tienda: Tienda): Observable<Tienda> {
    return this.http.put<Tienda>(this.url + '/' + tienda.id, tienda, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        if(e.error.exception == 'MethodArgumentNotValidException') {
          return throwError(e);
        }
        swal.fire('Error al editar la tienda', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  delete(id): Observable<Tienda> {
    return this.http.delete<Tienda>(`${this.url}/${id}`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

}
