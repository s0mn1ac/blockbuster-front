import { Component, OnInit } from '@angular/core';
import { Tienda } from './tienda';
import { TiendaService } from './tienda.service';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html',
  styleUrls: ['./tiendas.component.css']
})
export class TiendasComponent implements OnInit {
  
  tiendas: Tienda[];
  tpaginador: any;
  showID: boolean = true;

  constructor(private tiendaService: TiendaService, private activatedRoute: ActivatedRoute) { }
  
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');
      if(!page) {
        page = 0;
      }
      this.tiendaService
        .getTiendas(page)
        .subscribe(response => {
          this.tiendas = response.content as Tienda[];
          this.tpaginador = response;
      });
    });
  }

  switchID(): void {
    this.showID = !this.showID;
  }

  delete(tienda: Tienda): void {
    swal.fire({
      title: '¿Estás seguro?',
      text: "¡No podrás revertir los cambios!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '¡Sí, eliminar!',
      cancelButtonText: '¡No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.tiendaService.delete(tienda.id).subscribe(
          response => {
            this.tiendas = this.tiendas.filter(tie => tie !== tienda)
            swal.fire(
              '¡Eliminada!',
              `La tienda ${tienda.nombre} ha sido eliminada con éxito`,
              'success'
            )
          }
        )  
      } 
    })
  }

}
