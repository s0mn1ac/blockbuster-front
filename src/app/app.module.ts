import { BrowserModule } from "@angular/platform-browser";
import { NgModule, LOCALE_ID } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { ClientesComponent } from "./clientes/clientes.component";
import { ClienteService } from "./clientes/cliente.service";
import { TiendasComponent } from './tiendas/tiendas.component';
import { TiendaService } from './tiendas/tienda.service';
import { DesarrolladorasComponent } from './desarrolladoras/desarrolladoras.component';
import { DesarrolladoraService } from './desarrolladoras/desarrolladora.service';
import { JuegosComponent } from './juegos/juegos.component';
import { JuegoService } from './juegos/juego.service';
import { CFormComponent } from './clientes/cform/cform.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import localeES from '@angular/common/locales/es'
import { registerLocaleData } from '@angular/common';
import { TformComponent } from './tiendas/tform/tform/tform.component';
import { DformComponent } from './desarrolladoras/dform/dform.component';
import { JformComponent } from './juegos/jform/jform.component';
import { CdetailsComponent } from './clientes/cdetails/cdetails.component';
import { DdetailsComponent } from './desarrolladoras/ddetails/ddetails.component';

import { PaginatorComponent } from './paginator/paginator.component';
import { DpaginatorComponent } from './paginator/dpaginator/dpaginator.component';
import { JpaginatorComponent } from './paginator/jpaginator/jpaginator.component';
import { TpaginatorComponent } from './paginator/tpaginator/tpaginator.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { PedpaginatorComponent } from './paginator/pedpaginator/pedpaginator.component';
import { PedformComponent } from './pedidos/pedform/pedform.component';
import { LoginComponent } from './login/login/login.component';

registerLocaleData(localeES, 'es');

const routes: Routes = [
  { path: "login", component: LoginComponent },

  { path: "", redirectTo: "/pedidos", pathMatch: "full" },
  
  { path: "pedidos", component: PedidosComponent },
  { path: "pedidos/page/:page", component: PedidosComponent },

  { path: "clientes", component: ClientesComponent },
  { path: "clientes/page/:page", component: ClientesComponent },
  { path: "clientes/form", component: CFormComponent },
  { path: "clientes/form/:id", component: CFormComponent },
  
  { path: "tiendas", component: TiendasComponent },
  { path: "tiendas/page/:page", component: TiendasComponent },
  { path: "tiendas/form", component: TformComponent },
  { path: "tiendas/form/:id", component: TformComponent },
  
  { path: "juegos", component: JuegosComponent },
  { path: "juegos/page/:page", component: JuegosComponent },
  { path: "juegos/form", component: JformComponent },
  { path: "juegos/form/:id", component: JformComponent },
  
  { path: "desarrolladoras", component: DesarrolladorasComponent },
  { path: "desarrolladoras/page/:page", component: DesarrolladorasComponent },
  { path: "desarrolladoras/form", component: DformComponent },
  { path: "desarrolladoras/form/:id", component: DformComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ClientesComponent,
    TiendasComponent,
    DesarrolladorasComponent,
    JuegosComponent,
    CFormComponent,
    TformComponent,
    DformComponent,
    JformComponent,
    PaginatorComponent,
    CdetailsComponent,
    DdetailsComponent,
    DpaginatorComponent,
    JpaginatorComponent,
    TpaginatorComponent,
    PedidosComponent,
    PedpaginatorComponent,
    PedformComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule, 
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatMomentDateModule,
    MatDatepickerModule

  ],
  providers: [ClienteService, TiendaService, DesarrolladoraService, JuegoService, { provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule {}
