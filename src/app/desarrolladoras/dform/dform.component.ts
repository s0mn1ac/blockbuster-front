import { Component, OnInit } from '@angular/core';
import { Desarrolladora } from '../desarrolladora';
import { DesarrolladoraService } from '../desarrolladora.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dform',
  templateUrl: './dform.component.html',
  styleUrls: ['./dform.component.css']
})
export class DformComponent implements OnInit {

  desarrolladora: Desarrolladora = new Desarrolladora();
  titulo: string = 'Crear Desarrolladora';
  errores: string[];

  constructor(private desarrolladoraService: DesarrolladoraService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargar();
  }

  public cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id) {
        this.desarrolladoraService.getDesarrolladora(id).subscribe( (desarrolladora) => this.desarrolladora = desarrolladora )
      }
    })
  }

  public create(): void {
    this.desarrolladoraService.create(this.desarrolladora).subscribe(
      desarrolladora => {
        this.router.navigate(['/desarrolladoras'])
        swal.fire('Nueva desarrolladora', `¡Desarrolladora ${desarrolladora.nombre} creada con éxito!`, 'success')
      },
      e => {
        this.errores = e.error.valids as string[];
      }
    );
  }

  public update(): void {
    this.desarrolladoraService.update(this.desarrolladora).subscribe(
      desarrolladora => {
        this.router.navigate(['/desarrolladoras'])
        swal.fire('Desarrolladora actualizada', `¡Desarrolladora ${desarrolladora.nombre} actualizada con éxito!`, 'success')
      },
      e => {
        this.errores = e.error.valids as string[];
      }
    )
  }

}
