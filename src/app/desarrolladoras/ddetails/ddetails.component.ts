import { Component, OnInit, Input } from '@angular/core';
import { Desarrolladora } from '../desarrolladora';
import { DesarrolladoraService } from '../desarrolladora.service';
import { DmodalService } from './dmodal.service';
import { HttpEventType } from '@angular/common/http';
import swal from 'sweetalert2';

@Component({
  selector: 'details-desarrolladora',
  templateUrl: './ddetails.component.html',
  styleUrls: ['./ddetails.component.css']
})
export class DdetailsComponent implements OnInit {

  titulo: string = "Detalle de la desarrolladora";
  @Input() desarrolladora: Desarrolladora;
  imgSelected: File;
  progreso: number = 0;

  constructor(private desarrolladoraService: DesarrolladoraService, public dmodalService: DmodalService) { }

  ngOnInit(): void {}

  selectImg(event) {
    this.imgSelected = event.target.files[0];
    this.progreso = 0;
    if(this.imgSelected.type.indexOf('image') < 0) {
      swal.fire('Error en subida', 'Debes seleccionar una foto.', 'error');
      this.imgSelected = null;
    }
  }

  subirFoto() {
    if(!this.selectImg) {
      swal.fire('Error en subida', 'Debes seleccionar una foto.', 'error');
    } else {
      this.desarrolladoraService.uploadImg(this.imgSelected, this.desarrolladora.id).subscribe(
      event => {
        if(event.type === HttpEventType.UploadProgress) {
          this.progreso = Math.round((event.loaded/event.total)*100);
        } else if(event.type === HttpEventType.Response) {
          let response: any = event.body;
          this.desarrolladora = response as Desarrolladora;
          this.dmodalService.notifyUploadD.emit(this.desarrolladora);
          swal.fire('Subida completada', '¡La foto se ha subido con éxito!', 'success');
        }
      })
    }
  }

  closeDetails() {
    this.dmodalService.closeModal();
    this.imgSelected = null;
    this.progreso = 0;
  }

}
