import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DmodalService {

  modal: boolean = false;
  private _notifyUploadD = new EventEmitter<any>();

  constructor() { }

  get notifyUploadD(): EventEmitter<any> {
    return this._notifyUploadD;
  }

  openModal() {
    this.modal = true;
  }

  closeModal() {
    this.modal = false;
  }

}
