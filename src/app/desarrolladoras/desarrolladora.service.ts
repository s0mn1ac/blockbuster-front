import { Injectable } from "@angular/core";
import { formatDate } from '@angular/common';
import { Desarrolladora } from "./desarrolladora";
import { Observable, throwError } from "rxjs";
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { map, catchError } from "rxjs/operators";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from '../login/login/auth.service';

@Injectable()
export class DesarrolladoraService {

  private url: string = 'http://localhost:8090/desarrolladoras';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  
  constructor(private http: HttpClient, private router: Router, private authService: AuthService) {}

  private addHeaderAuthorization() {
    let token = this.authService.token;
    if(token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isAuthorized(e): boolean {
    
    if(e.status == 401) {
      this.router.navigate(['/login']);
      return true;
    }

    if(e.status == 403) {
      this.router.navigate(['/pedidos']);
      swal.fire('Acceso denegado', 'Este apartado solamente está disponible para administradores', 'warning');
      return true;
    }

    return false;

  }
  
  getDesarrolladoras(page: number): Observable<any> {
    return this.http.get<Desarrolladora[]>(this.url + '/page/' + page, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {

        if(this.isAuthorized(e)) {
          return throwError(e);
        }
      }),
      map(
        (response: any) => {
          (response.content as Desarrolladora[]).map(desarrolladora => {
            return desarrolladora;
          });
          return response;
        }
      )
    );
  }

  getDesarrolladora(id): Observable<Desarrolladora> {
    return this.http.get<Desarrolladora>(this.url + '/' + id, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        this.router.navigate(['/desarrolladoras']);
        swal.fire('Error al editar', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  create(desarrolladora: Desarrolladora): Observable<Desarrolladora> {
    return this.http.post<Desarrolladora>(this.url, desarrolladora, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        if(e.error.exception == 'MethodArgumentNotValidException') {
          return throwError(e);
        }
        swal.fire('Error al crear la desarrolladora', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  update(desarrolladora: Desarrolladora): Observable<Desarrolladora> {
    return this.http.put<Desarrolladora>(this.url + '/' + desarrolladora.id, desarrolladora, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        if(e.error.exception == 'MethodArgumentNotValidException') {
          return throwError(e);
        }
        swal.fire('Error al editar la desarrolladora', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  delete(id): Observable<Desarrolladora> {
    return this.http.delete<Desarrolladora>(`${this.url}/${id}`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

  uploadImg(file: File, id): Observable<HttpEvent<{}>> {
    
    let formData = new FormData();
    formData.append("file", file);
    formData.append("id", id);

    let httpHeaders = new HttpHeaders();
    let token = this.authService.token;
    if(token != null) {
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    }

    const req = new HttpRequest('POST', `${this.url}/upload`, formData, {
      reportProgress: true,
      headers: httpHeaders
    });
    
    return this.http.request(req).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );

  }

}
