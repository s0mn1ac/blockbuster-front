import { Component, OnInit } from '@angular/core';
import { Desarrolladora } from "./desarrolladora";
import { DesarrolladoraService } from "./desarrolladora.service";
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { DmodalService } from './ddetails/dmodal.service';

@Component({
  selector: 'app-desarrolladoras',
  templateUrl: './desarrolladoras.component.html',
  styleUrls: ['./desarrolladoras.component.css']
})
export class DesarrolladorasComponent implements OnInit {
  
  desarrolladoras: Desarrolladora[];
  dpaginador: any;
  showID: boolean = true;
  desarrolladoraSelected: Desarrolladora;
  
  constructor(private desarrolladoraService: DesarrolladoraService, private activatedRoute: ActivatedRoute, private dmodalService: DmodalService) { }
  
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');
      if(!page) {
        page = 0;
      }
      this.desarrolladoraService
        .getDesarrolladoras(page)
        .subscribe(response => {
          this.desarrolladoras = response.content as Desarrolladora[];
          this.dpaginador = response;
        });
    });
    this.dmodalService.notifyUploadD.subscribe(desarrolladora => {
      this.desarrolladoras = this.desarrolladoras.map(desarrolladoraOriginal => {
        if(desarrolladora.id == desarrolladoraOriginal.id){
          desarrolladoraOriginal.img = desarrolladora.img;
        }
        return desarrolladoraOriginal;
      })
    })
  }

  switchID(): void {
    this.showID = !this.showID;
  }

  delete(desarrolladora: Desarrolladora): void {
    swal.fire({
      title: '¿Estás seguro?',
      text: "¡No podrás revertir los cambios!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '¡Sí, eliminar!',
      cancelButtonText: '¡No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.desarrolladoraService.delete(desarrolladora.id).subscribe(
          response => {
            this.desarrolladoras = this.desarrolladoras.filter(des => des !== desarrolladora)
            swal.fire(
              '¡Eliminada!',
              `La desarrolladora ${desarrolladora.nombre} ha sido eliminada con éxito`,
              'success'
            )
          }
        )  
      } 
    })
  }

  openDetails(desarrolladora: Desarrolladora) {
    this.desarrolladoraSelected = desarrolladora;
    this.dmodalService.openModal();
  }

}