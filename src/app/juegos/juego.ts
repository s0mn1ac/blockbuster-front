import { Desarrolladora } from 'src/app/desarrolladoras/desarrolladora';

export class Juego {
    id: number;
    titulo: string;
    fechaLanzamiento: Date;
    categoria: string;
    pegi: number;
    desarrolladoras: Desarrolladora[];
}
