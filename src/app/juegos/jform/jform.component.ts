import { Component, OnInit } from '@angular/core';
import { Juego } from '../juego';
import { JuegoService } from '../juego.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Desarrolladora } from 'src/app/desarrolladoras/desarrolladora';

@Component({
  selector: 'app-jform',
  templateUrl: './jform.component.html',
  styleUrls: ['./jform.component.css']
})
export class JformComponent implements OnInit {

  desarrolladoras: Desarrolladora[];
  juego: Juego = new Juego();
  titulo: string = 'Crear Juego';
  errores: string[];  

  constructor(private juegoService: JuegoService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargar();
    this.cargarDesarrolladoras();
  }

  public cargar(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id) {
        this.juegoService.getJuego(id).subscribe( (juego) => this.juego = juego )
      }
    })
  }

  private cargarDesarrolladoras(): void {
    this.juegoService.getDesarrolladoras().subscribe(desarrolladoras => this.desarrolladoras = desarrolladoras);
  }

  public compararDesarrolladora(o1: Desarrolladora, o2: Desarrolladora): boolean {
    return o1 == null || o2 == null ? false : o1.nombre == o2.nombre;
  }

  public create(): void {
    this.juegoService.create(this.juego).subscribe(
      juego => {
        this.router.navigate(['/juegos'])
        swal.fire('Nuevo juego', `¡Juego ${juego.titulo} creado con éxito!`, 'success')
      },
      e => {
        this.errores = e.error.valids as string[];
      }
    );
  }

  public update(): void {
    this.juegoService.update(this.juego).subscribe(
      juego => {
        this.router.navigate(['/juegos'])
        swal.fire('Juego actualizado', `¡Juego ${juego.titulo} actualizado con éxito!`, 'success')
      },
      e => {
        this.errores = e.error.valids as string[];
      }
    )
  }

}
