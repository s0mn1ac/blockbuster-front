import { Injectable } from "@angular/core";
import { formatDate } from '@angular/common';
import { Juego } from "./juego";
import { Observable, throwError } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Desarrolladora } from '../desarrolladoras/desarrolladora';
import { map, catchError } from "rxjs/operators";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from '../login/login/auth.service';

@Injectable()
export class JuegoService {
  
  private url: string = 'http://localhost:8090/juegos';
  private urlDesarrolladoras: string = 'http://localhost:8090/desarrolladoras';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  
  constructor(private http: HttpClient, private router: Router, private authService: AuthService) {}

  private addHeaderAuthorization() {
    let token = this.authService.token;
    if(token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isAuthorized(e): boolean {
    
    if(e.status == 401) {
      this.router.navigate(['/login']);
      return true;
    }

    if(e.status == 403) {
      this.router.navigate(['/pedidos']);
      swal.fire('Acceso denegado', 'Este apartado solamente está disponible para administradores', 'warning');
      return true;
    }

    return false;

  }
  
  getJuego(id): Observable<Juego> {
    return this.http.get<Juego>(this.url + '/' + id, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        this.router.navigate(['/juegos']);
        swal.fire('Error al editar', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  getJuegos(page: number): Observable<any> {
    return this.http.get<Juego[]>(this.url + '/page/' + page, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
      }),
      map(
        (response: any) => {
          (response.content as Juego[]).map(juego => {
            return juego;
          });
          return response;
        }
      )
    );
  }

  create(juego: Juego): Observable<Juego> {
    return this.http.post<Juego>(this.url, juego, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        if(e.error.exception == 'MethodArgumentNotValidException') {
          return throwError(e);
        }
        swal.fire('Error al crear el juego', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  update(juego: Juego): Observable<Juego> {
    return this.http.put<Juego>(this.url + '/' + juego.id, juego, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
        if(e.error.exception == 'MethodArgumentNotValidException') {
          return throwError(e);
        }
        swal.fire('Error al editar el juego', e.error.message, 'error');
        return throwError(e);
      })
    );
  }

  delete(id): Observable<Juego> {
    return this.http.delete<Juego>(`${this.url}/${id}`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

  getDesarrolladoras(): Observable<Desarrolladora[]> {
    return this.http.get<Desarrolladora[]>(this.urlDesarrolladoras, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

}