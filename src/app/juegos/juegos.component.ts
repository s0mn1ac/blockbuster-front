import { Component, OnInit } from '@angular/core';
import { Juego } from "./juego";
import { JuegoService } from "./juego.service";
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {
  
  juegos: Juego[];
  jpaginador: any;
  showID: boolean = true;
  
  constructor(private juegoService: JuegoService, private activatedRoute: ActivatedRoute) { }
  
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');
      if(!page) {
        page = 0;
      }
      this.juegoService
        .getJuegos(page)
        .subscribe(response => {
          this.juegos = response.content as Juego[];
          this.jpaginador = response;
      });
    });
  }

  switchID(): void {
    this.showID = !this.showID;
  }

  delete(juego: Juego): void {
    swal.fire({
      title: '¿Estás seguro?',
      text: "¡No podrás revertir los cambios!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '¡Sí, eliminar!',
      cancelButtonText: '¡No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.juegoService.delete(juego.id).subscribe(
          response => {
            this.juegos = this.juegos.filter(jue => jue !== juego)
            swal.fire(
              '¡Eliminado!',
              `El juego ${juego.titulo} ha sido eliminado con éxito`,
              'success'
            )
          }
        )  
      } 
    })
  }

}