import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from "rxjs/operators";
import { Pedido } from './pedido';

import swal from 'sweetalert2';
import { Juego } from '../juegos/juego';
import { Cliente } from '../clientes/cliente';
import { Tienda } from '../tiendas/tienda';
import { AuthService } from '../login/login/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  private url: string = 'http://localhost:8090';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient, private router: Router, private authService: AuthService) { }

  private addHeaderAuthorization() {
    let token = this.authService.token;
    if(token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isAuthorized(e): boolean {
    
    if(e.status == 401) {
      this.router.navigate(['/login']);
      return true;
    }

    if(e.status == 403) {
      this.router.navigate(['/pedidos']);
      swal.fire('Acceso denegado', 'Este apartado solamente está disponible para administradores', 'warning');
      return true;
    }

    return false;

  }

  getPedidos(page: number): Observable<any> {
    return this.http.get<Pedido[]>(`${this.url}/pedidos/page/${page}`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        if(this.isAuthorized(e)) {
          return throwError(e);
        }
      }),
      map( (response: any) => {
        (response.content as Pedido[]).map(pedido => {
          return pedido;
        });
        return response;
      })
    );
  }
  
  delete(id): Observable<Pedido> {
    return this.http.delete<Pedido>(`${this.url}/pedidos/${id}`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

  getJuegos(): Observable<Juego[]> {
    return this.http.get<Juego[]>(`${this.url}/juegos`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(`${this.url}/clientes`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

  getTiendas(): Observable<Tienda[]> {
    return this.http.get<Tienda[]>(`${this.url}/tiendas`, {headers: this.addHeaderAuthorization()}).pipe(
      catchError(e => {
        this.isAuthorized(e);
        return throwError(e);
      })
    );
  }

}
