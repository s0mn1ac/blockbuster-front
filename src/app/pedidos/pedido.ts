import { Juego } from '../juegos/juego';
import { Cliente } from '../clientes/cliente';
import { Tienda } from '../tiendas/tienda';

export class Pedido {
    id: number;
    codReferencia: number;
    estado: string;
    juego: Juego;
    cliente: Cliente;
    tienda: Tienda;
}