import { Component, OnInit } from '@angular/core';
import { Pedido } from './pedido';
import { PedidosService } from './pedidos.service';
import { ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';
import { AuthService } from '../login/login/auth.service';

@Component({
  selector: 'pedidos-section',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {

  pedidos: Pedido[];
  pedidosPaginator: any;
  showID: boolean = true;

  constructor(private pedidosService: PedidosService, private activatedRoute: ActivatedRoute, private authService: AuthService) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      
      let page: number = +params.get('page');
      
      if(!page) {
        page = 0;
      }

      this.pedidosService
        .getPedidos(page)
        .subscribe(response => {
          this.pedidos = response.content as Pedido[];
          this.pedidosPaginator = response;
        }
      );

    });
  }


  switchID(): void {
    this.showID = !this.showID;
  }


  delete(pedido: Pedido): void {
    swal.fire({
      title: '¿Estás seguro?',
      text: "¡No podrás revertir los cambios!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '¡Sí, eliminar!',
      cancelButtonText: '¡No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.pedidosService.delete(pedido.id).subscribe(
          response => {
            this.pedidos = this.pedidos.filter(ped => ped !== pedido)
            swal.fire(
              '¡Eliminado!',
              `El pedido ${pedido.codReferencia} ha sido eliminado con éxito`,
              'success'
            )
          }
        )  
      } 
    })
  }

}
