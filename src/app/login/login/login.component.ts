import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import swal from 'sweetalert2';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  titulo: string = 'Por favor, inicie sesión';
  user: User;

  constructor(private authService: AuthService, private router: Router) { this.user = new User(); }

  ngOnInit(): void {
    if(this.authService.isAuthenticated()) {
      swal.fire('Oops', `Parece que ya estás autenticado, ${this.authService.user.name}`, 'info');
      this.router.navigate(['/clientes']);
    }
  }

  login(): void {

    console.log(this.user);

    if(this.user.username == null || this.user.password == null) {
      swal.fire('Error al iniciar sesión', 'Los campos Usuario y Contraseña no pueden estar vacíos', 'error');
      return;
    }

    this.authService.login(this.user).subscribe(response => {
      //console.log(response);

      this.authService.saveUser(response.access_token);
      this.authService.saveToken(response.access_token);
      
      let user = this.authService.user;

      this.router.navigate(['/pedidos']);
      swal.fire('Inicio de sesión correcto', `¡Bienvenido, ${user.name}!`, 'success');

    }, e => {
      if(e.status == 400) {
        swal.fire('Error al iniciar sesión', 'El nombre de usuario o la contraseña no son correctos', 'error');
      }
    })

  }

}
