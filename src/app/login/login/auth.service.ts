import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _user: User;
  private _token: string;

  constructor(private http: HttpClient) { }

  public get user(): User {
    if(this._user != null) {
      return this._user;
    } else if(this._user == null && sessionStorage.getItem('user') != null) {
      this._user = JSON.parse(sessionStorage.getItem('user')) as User;
      return this._user;
    }
    return new User();
  }

  public get token(): string {
    if(this._token != null) {
      return this._token;
    } else if(this._token == null && sessionStorage.getItem('token') != null) {
      this._token = sessionStorage.getItem('token');
      return this._token;
    }
    return null;
  }

  login(user: User): Observable<any> {
    
    const url = 'http://localhost:8090/oauth/token';
    const cred = btoa('angularapp' + ':' + '12345');
    const httpHeaders = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Basic ' + cred});
    
    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('name', user.name);
    params.set('username', user.username);
    params.set('img', user.img);
    params.set('password', user.password);

    console.log(params.toString());

    return this.http.post(url, params.toString(), {headers: httpHeaders});

  }

  logout(): void {
    this._token = null;
    this._user = null;
    sessionStorage.clear();
  }

  saveUser(accessToken: string): void {
    let payload = this.extractToken(accessToken);
    this._user = new User();
    this._user.name = payload.name;
    this._user.username = payload.user_name;
    this._user.img = payload.img;
    this._user.roles = payload.authorities;
    sessionStorage.setItem('user', JSON.stringify(this._user));
  }

  saveToken(accessToken: string): void {
    this._token = accessToken;
    sessionStorage.setItem('token', accessToken);
  }

  extractToken(accessToken: string): any {
    if(accessToken != null) {
      return JSON.parse(atob(accessToken.split('.')[1]));
    }
    return null;
  }

  isAuthenticated(): boolean {
    
    let payload = this.extractToken(this.token);
    
    if(payload != null && payload.user_name && payload.user_name.length > 0) {
      return true;
    }

    return false;

  }

  hasRole(role: string): boolean {
    if(this.user.roles.includes(role)) {
      return true;
    }
    return false;
  }

}
